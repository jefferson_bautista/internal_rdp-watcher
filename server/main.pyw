import subprocess, os, sys
import csv
import json
import re

import time
import config

from utils import stringify

def find_account(username, session):
    for row in session:
        row = [re.sub(r'.*\\', '' , r).lower() for r in row]

        if username in row:
            for i, col in enumerate(row):
                re_ip = r'^((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$'
                if re.fullmatch(re_ip, col):
                    ip = re.fullmatch(re_ip, col).group()
                    clientname = row[i+1]
                    return {'clientip': ip, 'clientname': clientname}
    return {'clientip':'', 'clientname':''}


def run():
    with open(config.DUMPFILE_SERVER_REMOTE, 'r') as json_:
        servers = json.load(json_)

    json_data = servers

    for x, server in enumerate(servers):

        p = subprocess.Popen("query session /server:{}".format(server['servername']), stdout=subprocess.PIPE, shell=True)
        stdout, stderr = p.communicate()

        session = [x.split() for x in stdout.decode('ascii').splitlines()]
        
        #print("Current server: ", server['servername'])
        for data in session:
            for i, account in enumerate(server.get('accounts',[])):
                #print(account.get('username','').lower() , " in ", [d.lower() for d in data])
                if account.get('username','').lower() in [d.lower() for d in data]:
                    json_data[x]['accounts'][i]['status'] = [d.lower() for d in data if d.lower() == 'active' or d.lower() == 'disc'][0]
                    

    for x,machine in enumerate(json_data):
        script = "get-server.ps1"
        server = machine['servername']

        p = subprocess.Popen(["PowerShell.exe", 
                            "{}\\{} {} | Format-Table -Wrap -AutoSize".format(os.getcwd(), script, server)], stdout=subprocess.PIPE, shell=True)

        stdout,stderr = p.communicate()
        session = [x.split() for x in stdout.decode('ascii').splitlines()]

        for i,account in enumerate(machine['accounts']):
            if account.get('status', '') == 'active':
                json_data[x]['accounts'][i].update(find_account(account['username'], session))

    with open(config.DUMPFILE_DATA_REMOTE, 'w') as f:
        f.write(stringify(json_data))
    print("Done")

while True:
    run()
    time.sleep(8)
