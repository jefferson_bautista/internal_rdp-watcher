import subprocess
import csv
import json

import config

from utils import stringify


configuration = config.RemoteConfig()

with open(configuration.server_dumpfile, 'r') as f:
    servers = f.read().splitlines()

json_data = []

for server in servers:

    p = subprocess.Popen("query session /server:{}".format(server), stdout=subprocess.PIPE, shell=True)
    stdout, stderr = p.communicate()

    session = [x.split() for x in stdout.decode('ascii').splitlines()]
    json_data.append({"server":server, "accounts":[]})

    for data in session:
        username = [d.lower() for d in data if d.lower() == 'analancefiles' or d.lower() == 'analanceadmin']
        status = [d.lower() for d in data if d.lower() == 'active' or d.lower() == 'disc']

        if len(username) == 0:
            continue
        if len(status) == 0:
            status = 'unknown'

        json_data[-1]["accounts"].append(dict(username=username[0], status=status[0]))

with open(configuration.data_dumpfile, 'w') as f:
    f.write(stringify(json_data))

    


