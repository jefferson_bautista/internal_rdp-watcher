import subprocess, sys, os

script = "get-server.ps1"
server = "cwypwd-50.on.bell.ca"

p = subprocess.Popen(["PowerShell.exe", 
                    "{}\\{} {} | Format-Table -Wrap -AutoSize".format(os.getcwd(), script, server)], stdout=subprocess.PIPE, shell=True)

stdout,stderr = p.communicate()
session = [x.split() for x in stdout.decode('ascii').splitlines()]
print(session)