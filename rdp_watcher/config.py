import os
import json


APP_ROOT = os.path.dirname(os.path.abspath(__file__))   
APP_STATIC = os.path.join(APP_ROOT, 'static')

with open('{}/config.json'.format(APP_ROOT)) as json_data:
    config = json.load(json_data)

DUMPFILE_SERVER_LOCAL = config["DUMPFILE_SERVER_LOCAL"]
DUMPFILE_SERVER_REMOTE = config["DUMPFILE_SERVER_REMOTE"]
DUMPFILE_DATA_LOCAL = config["DUMPFILE_DATA_LOCAL"]
DUMPFILE_DATA_REMOTE = config["DUMPFILE_DATA_REMOTE"]
PATH_REMOTE = config["PATH_REMOTE"]