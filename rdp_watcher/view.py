from flask import Flask, render_template, jsonify, request
from rdp_watcher import app
from rdp_watcher import config

import os
import json

from rdp_watcher.utils import stringify

@app.route("/")
def index():
    return render_template("index.html")


@app.route("/refresh" , methods=['GET', 'POST'])
def refresh():
    data = get_data()
    return json.dumps(data)


@app.route("/add_server")
def add_server():
    servername = request.args.get('servername')

    command = "net use {}".format(config.PATH_REMOTE)
    os.system(command)
    
    try:
        with open(config.DUMPFILE_SERVER_REMOTE, 'r') as r:
            servers = json.loads(r.read())

        servers.append(
            {"servername": str(servername),
                        "ip":"",
                        "accounts":[
                            {"username": "analancefiles"},
                            {"username": "analanceadmin"}
                        ]
            }
        )

        str_server = stringify(servers)

        with open(config.DUMPFILE_SERVER_REMOTE, 'w') as w:
            w.write(str_server)

    except PermissionError:
        print("Cannot access file")
    except Exception as ex:
        print("Accessing dumpfile throws : ",ex)

    return "Done"


def get_data():
    command = "net use {}".format(config.PATH_REMOTE)
    os.system(command)
    try:
        with open(config.DUMPFILE_DATA_REMOTE, 'r') as r:
            return json.loads(r.read())
    except PermissionError:
        print("Cannot access file")
    except Exception as ex:
        print("Accessing dumpfile throws : ",ex)
    return None